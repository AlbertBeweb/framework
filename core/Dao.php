<?php
    /*
    * le dao est un objet servant de couche intermediare entre la couche metier et la db
    * cet objet ne doit exister qu'en un seul exemplaire
    */

    abstract class Dao implements Crud_interface, Repository_interface{
        
        protected $pdo;

        function __construct(){
            $this->pdo = new PDO("mysql:host=localhost;dbname=RESTO_DB_BWB","nicolas","");

        }
    }