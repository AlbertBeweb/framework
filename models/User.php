<?php
/**
 * Entité user
 */

    class User {
        private $id;
        private $username;
        private $password;
        private $email;

        public function __construct($id=null,$username=null,$password=null,$email=null){
            $this->id =$id;
            $this->email =$email;
            $this->password =$password;
            $this->username =$username;
        }

        
        public function to_array(){
            $array = array(
                "id"=>       $this->id,
                "email"=>    $this->email,
                "password"=> $this->password,
                "username"=> $this->username
            );
            return $array;
        }

        public function to_json(){
            
            return json_encode($this->to_array());
        }

        /**
         * Get the value of id
         */ 
        public function getId(){
                return $this->id;
        }

        /**
         * Set the value of id
         *
         * @return  self
         */ 
        public function setId($id){
                $this->id = $id;

                return $this;
        }

        /**
         * Get the value of username
         */ 
        public function getUsername(){
                return $this->username;
        }

        /**
         * Set the value of username
         *
         * @return  self
         */ 
        public function setUsername($username){
                $this->username = $username;

                return $this;
        }

        /**
         * Get the value of password
         */ 
        public function getPassword(){
                return $this->password;
        }

        /**
         * Set the value of password
         *
         * @return  self
         */ 
        public function setPassword($password){
                $this->password = $password;

                return $this;
        }

        /**
         * Get the value of email
         */ 
        public function getEmail(){
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */ 
        public function setEmail($email){
                $this->email = $email;

                return $this;
        }
};